package main

import (
	"os"
	"log"
	"fmt"
	"strconv"
	"time"
	"bytes"
	"github.com/faiface/beep"
	"github.com/faiface/beep/vorbis"
	"github.com/faiface/beep/speaker"
)


func main() {

	if len(os.Args) < 3 || os.Args[1] != "-t"  && os.Args[1] != "-s"{
		log.Print("Usage is as follows, -t <TIMEINMINUTES>")
    		log.Print("Or, -s <TIMEINMINUTES> plays a gong at the beginning")
		os.Exit(1)
	}

	var buffer bytes.Buffer
	done := make(chan bool)
	log.New(&buffer, "pomolog", log.Llongfile)
	log.Print("\033[38:2:200:0:0mStarting timer.\033[0m")
	start := time.Now()
	fmt.Println("Welcome to pomodoro!")
  var gongstreamer beep.Streamer
  if os.Args[1] == "-s" {
    gong, err := os.Open("sounds/gong.ogg")
    if err != nil {
      panic(err)
    }
    log.Print("\033[38:0:255:0mLET THE CODING BEGIN\033[0m")
    gongstreamer, _, err = vorbis.Decode(gong)
    if err != nil {
      panic(err)
    }
    defer gong.Close()

  }


	file, err := os.Open("sounds/ping.ogg")
	if err != nil {
		panic(err)
	}
	log.Print("\033[38:3:0:100:0mOpened sound file\033[0m")
	streamer, format, err := vorbis.Decode(file)
	if err != nil {
		panic(err)
	}
	defer streamer.Close()
	log.Print("\033[38:3:10:100:0mDecoded sound file\033[0m")

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))


	arg, err := strconv.Atoi(os.Args[2])
	if err != nil {
		panic(err)
	}

	var notDone bool
	notDone = true
	minuteTicker := time.NewTicker(time.Minute)
  if os.Args[1] == "-s"{
    speaker.Play(beep.Seq(gongstreamer, beep.Callback(func() {
      done <- false
    })))

  }
	for notDone {
		currentTime := time.Since(start)

		select {
		case <-minuteTicker.C:
			log.Print("\033[38:2:0:255:0m"+currentTime.String()+" have passsed\033[0m")
		}
		if currentTime.Minutes() >= float64(arg) {
			log.Print("\033[38:2:255:0:0mTime's up!\033[0m")
			speaker.Play(beep.Seq(streamer,  beep.Callback(func() {
			done <- false
			})))
		select {
		case notDone = <-done:
			notDone = false
		}
		}
	}

}
